<?php
ini_set('error_reporting', E_ALL);
include_once ("conf.inc");
include_once ("class.php");

$Astra = new AstraMon($CONFIG["MYSQL_HOST"], $CONFIG["MYSQL_LOGIN"], $CONFIG["MYSQL_PASS"], $CONFIG["MYSQL_BD"], $CONFIG["MYSQL_PORT"]);
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" href="css/jquery-ui.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/menu.css" />
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.tablesorter.js"></script>
</head>

<body>
<div class="content">
	<div class="header_left">
		<h2 style="margin: 0px;">Astra Monitor</h2>
		<a href="http://www.cesbo.com" style="text-decoration: inherit;color: gray;">www.cesbo.com</a>
	</div>
	<div class="header_right">
	<!-- future -->
	</div>
	<div class="nav">
  		<a href="?page=channels" <?=((isset($_GET["page"]) && $_GET["page"]=='channels')? 'class="chosen"':'')?>>Каналы</a>
     	<a href="?page=adapters" <?=((isset($_GET["page"]) && $_GET["page"]=='adapters')? 'class="chosen"':'')?>>Адаптеры</a>
		<a href="?page=logs" <?=((isset($_GET["page"]) && $_GET["page"]=='logs')? 'class="chosen"':'')?>>Логи</a>
      	<a href="?page=servers" <?=((isset($_GET["page"]) && $_GET["page"]=='servers')? 'class="chosen"':'')?>>Сервера</a>
  	</div>
	<?php
    if (isset($_GET["page"])) {
        if (file_exists("include/" . $_GET["page"] . ".inc")) {
			require("include/" . $_GET["page"] . ".inc");
        } 
    }else{
       require("include/channels.inc");
    }
	?>

</div>
</body></html>