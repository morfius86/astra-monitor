<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

include_once ("conf.inc");
include_once ("class.php");
$Astra = new AstraMon($CONFIG["MYSQL_HOST"], $CONFIG["MYSQL_LOGIN"], $CONFIG["MYSQL_PASS"], $CONFIG["MYSQL_BD"], $CONFIG["MYSQL_PORT"]);


$json_request = json_decode(file_get_contents('php://input'), true);
$input_info = array_pop($json_request);		

if (isset($_GET["server"]) && strlen($_GET["server"]) > 0 && isset($_GET["conf"]) && strlen($_GET["conf"]) > 0 ){
	
	$Server=$Astra->get_server(array("server.server"=>$_GET["server"]));
	#сервер не найден
	if(!$Server){
		
		#создаем сервер
		if (!$Astra->create_server($_GET["server"])){
			#ошибка
		}
	}else{
		#открываем и парсим конфиг
		$parse = $Astra->parse_config_astra($_GET["conf"]);
		
		if ($parse != false){
			#обновиv
			$Astra->update_server_conf($_GET["server"], $_GET["conf"], 1, count($parse["dvb"]), count($parse["channel"]));
			/* CHANNEL */
			if (isset($input_info['channel_id'])) {
				
				if (isset($parse["channel"][$input_info['channel_id']])) {
					
					#Создаем/обновляем канал
					if (!$Astra->create_channel($_GET["server"], $input_info['channel_id'], $parse["channel"][$input_info["channel_id"]]["output"], $parse["channel"][$input_info["channel_id"]]["name"], 
					$parse["channel"][$input_info["channel_id"]]["input_stream"], $input_info['scrambled'], $input_info['cc_error'], $input_info['pes_error'], $input_info['bitrate'], $input_info['onair'])) {
						#ошибка
					}
				}
			}
			/* DVB */
			if (isset($input_info['dvb_id'])) {
				
				if (isset($parse["dvb"][$input_info['dvb_id']])) {
					
					#Создаем/обновляем тюнер
					if (!$Astra->create_adapter($input_info['dvb_id'], $parse["dvb"][$input_info["dvb_id"]]["adapter"], $_GET["server"], $parse["dvb"][$input_info["dvb_id"]]["dvb_tune"], 
					$input_info['status'], ($input_info['signal']*100/65535), ($input_info['snr']*100/65535), '', $input_info['unc'], $input_info['ber'])) {
						#ошибка
					}
				}
			}
			
		}else{
			#не удалось отпарсить
			$Astra->update_server_conf($_GET["server"], $_GET["conf"], 0);
		}
	}
}

?>
