<?php

Class AstraMon {
	public $mysql=false;
	public $mysql_res=false;
	private $filter_sql=false;
	 
	public function __construct($host, $user, $pass, $db, $port){
		if (!$this->mysql){
			$this->connect($host, $user, $pass, $db, $port);
		}
		$this->request($_REQUEST); 
	}
		
	public function connect($host, $user, $pass, $db, $port) {
		$this->mysql = new mysqli($host, $user, $pass, $db, $port); 
		if (!$this->mysql) {
			die('Ошибка соединения: ' . mysqli_connect_errno());
		}
		return true;
	}
	
	private function REQUEST($request) {
		#request delete
		if (isset($request["delete"])){
			if ($request["delete"]=='channels' && isset($request["select_channel"])) {
				$this->delete_channel($request["select_channel"]);
			}elseif($request["delete"]=='adapters' && isset($request["select_adapter"])){
				$this->delete_adapter($request["select_adapter"]);
			}elseif($request["delete"]=='servers' && isset($request["select_server"])){
				$this->delete_server($request["select_server"]);
			}elseif($request["delete"]=='logs'){
				$this->delete_logs();
			}elseif($request["delete"]=='astra_conf' && isset($request["select_conf"])){
				$this->delete_astra_config($request["select_conf"]);
			}
		}
	}
	
	private function filter_to_sql($filter) {
		if (is_array($filter)){
			$filter = array_diff($filter, array(''));
			#параметры выборки
			if (count($filter)>0) {
				$i=1;
				$Query=" WHERE ";
				foreach($filter as $filter_key => $filter_val) {
					$Query.="{$filter_key} = '{$filter_val}' ".($i==count($filter)? '':'AND ')."";
					$i++;
				}
				return $Query;
			}
		}
		return false;
	}
	
	/**** CHANNELS ****/
	public function create_channel($server, $channel_id, $output, $channel, $stream, $scrambled, $cc_error, $pes_error, $bitrate, $ready) {
		$scrambled=$scrambled?'1':'0';
		$ready=$ready?'1':'0';
		$query = "INSERT INTO channels (`server`, `channel_id`, `output`, `channel`, `stream`, `scrambled`, `cc_error`, `pes_error`, `bitrate`, `ready`) VALUES (
		'".$server."', '".$channel_id."', '".$output."', '".$channel."', '".$stream."', '".$scrambled."', '".$cc_error."','".$pes_error."','".$bitrate."','".$ready."') ON DUPLICATE KEY UPDATE 
		`server` = '".$server."', `output` = '".$output."',`channel` = '".$channel."',`stream` = '".$stream."',`scrambled` = '".$scrambled."',`cc_error` = '".$cc_error."',`pes_error` = '".$pes_error."',`bitrate` = '".$bitrate."',`ready` = '".$ready."',`last_update` = NOW()";
		if ($this->mysql_res = $this->mysql->query($query)) {
			return true;
		}
		return false;
	}
	
	public function get_channel($w='') {
		$return = array ();
		$query = "select channels.*, TIME_TO_SEC(timediff(now(),last_update)) as last_update_period from channels ".($this->filter_to_sql($w))." order by channels.server, channels.stream";
		if ($this->mysql_res=$this->mysql->query($query)) {
			while($row = mysqli_fetch_assoc($this->mysql_res)) {
				$return[]=$row;
			}
			$this->filter_sql=false;
			return $return;
		}
		return false;
	}
	
	public function get_channel_stream() {
		$query = "select channels.stream from `channels` group by channels.stream";
		if ($this->mysql_res=$this->mysql->query($query)) {
			while($row = mysqli_fetch_assoc($this->mysql_res)) {
				$return[]=$row;
			}
			return $return;
		}
		return false;
	}
	
	public function delete_channel($channels) {
		$return = array ();
		if (is_array($channels)){
			foreach ($channels as $id){
				if ($this->mysql_res = $this->mysql->query("DELETE  From channels where id = '{$id}'")) {
					$return[$id] = 'ok';
				}else{
					$return[$id] = 'error';
				}
			}
		}
		return $return;
	}
	/**** LOGS ****/
	public function get_logs($w='', $l='') {
		$return = array ();
		$query = "select log.* from log ".($this->filter_to_sql($w))." order by log.log_id DESC ".((!empty($l))? "LIMIT {$l}":"")."";
		if ($this->mysql_res=$this->mysql->query($query)) {
			while($row = mysqli_fetch_assoc($this->mysql_res)) {
				$return[]=$row;
			}
			$this->filter_sql=false;
			return $return;
		}
		return false;
	}
	
	public function delete_logs() {
		if ($this->mysql_res = $this->mysql->query("DELETE From log")) {
			return true;
		}else{
			return false;
		}
		return false;
	}
	
	
	
	/**** ADAPTERS ****/
	public function get_adapter($w='') {
		$return = array ();
		$query = "SELECT adapters.* , SUM( channels.bitrate ) AS bitrate_sum, TIME_TO_SEC(timediff(now(), adapters.last_update)) as last_update_period FROM  `adapters` 
		LEFT JOIN channels ON adapters.stream = channels.stream
		AND adapters.server = channels.server ".($this->filter_to_sql($w))."
		GROUP BY adapters.adapter_id
		ORDER BY adapters.server, adapters.adapter ";
		if ($this->mysql_res=$this->mysql->query($query)) {
			while($row = mysqli_fetch_assoc($this->mysql_res)) {
				$return[]=$row;
			}
			$this->filter_sql=false;
			return $return;
		}
		return false;
	}
	
	public function create_adapter($adapter_id, $adapter, $server,$stream, $lock, $signal, $snr, $bitrate, $unc, $ber) {
		$query = "INSERT INTO adapters (`adapter_id`, `adapter`, `server`, `stream`, `lock`, `signal`, `snr`, `bitrate`, `unc`, `ber`) VALUES (
		'".$adapter_id."', '".$adapter."', '".$server."', '".$stream."', '".$lock."', '".$signal."', '".$snr."', '".$bitrate."', '".$unc."', '".$ber."') ON DUPLICATE KEY UPDATE 
		`adapter` = '".$adapter."',`server` = '".$server."',`stream` = '".$stream."',`lock` = '".$lock."',`signal` = '".$signal."',`snr` = '".$snr."',`bitrate` = '".$bitrate."',`unc` = '".$unc."',`ber` = '".$ber."',`last_update` = NOW()";	
		if ($this->mysql_res = $this->mysql->query($query)) {
			return true;
		}
		return false;
	}
	
	public function delete_adapter($adapters) {
		$return = array ();
		if (is_array($adapters)){
			foreach ($adapters as $id){
				if ($this->mysql_res = $this->mysql->query("DELETE  From adapters where id = '{$id}'")) {
					$return[$id] = 'ok';
				}else{
					$return[$id] = 'error';
				}
			}
		}
		return $return;
	}
	
	/**** SERVERS ****/
	public function get_server($w='') {
		$return = false;
		$query = "select server.* from `server` ".($this->filter_to_sql($w))."";
		if ($this->mysql_res=$this->mysql->query($query)) {
			while($row = mysqli_fetch_assoc($this->mysql_res)) {
				$return[]=$row;
			}
		}
		return $return;
	}
	
	public function get_server_conf($server=null) {
		$return = false;
		$query = "select server_conf.* from `server_conf` ".(($server)? "Where server_conf.server = '{$server}'":"")."";
		if ($this->mysql_res=$this->mysql->query($query)) {
			while($row = mysqli_fetch_assoc($this->mysql_res)) {
				$return[]=$row;
			}
		}
		return $return;
	}
	
	public function create_server($server) {
		$query = "INSERT INTO server (`server`) VALUES ('{$server}')";
		if ($this->mysql_res = $this->mysql->query($query)) {
			return true;
		}
		return false;
	}
	
	public function delete_server($servers) {
		$return = array ();
		if (is_array($servers)){
			foreach ($servers as $id){
				if ($this->mysql_res = $this->mysql->query("DELETE From server where id = '{$id}'")) {
					$return[$id] = 'ok';
				}else{
					$return[$id] = 'error';
				}
			}
		}
		return $return;
	}
	
	public function update_server_conf($server, $conf, $res='NULL', $tune='NULL', $channel='NULL') {
		$query = "INSERT INTO server_conf (`server`, `conf`, `res`, `tune`, `channel`) VALUES ('{$server}','{$conf}', {$res}, {$tune}, {$channel}) ON DUPLICATE KEY UPDATE 
		`res` = {$res}, `tune` = {$tune}, `channel` = {$channel}, `last_update` = NOW()";
		if ($this->mysql_res = $this->mysql->query($query)) {
			return true;
		}
		return false;
	}	
	
	/**** CONFIG ****/
	public function parse_config_astra($config) {
		$PARAMS=false;
		$file = file_get_contents($config);
		preg_match_all('/([a-zA-Z0-9_]+)=dvb_tune\({(.*?)}\)/', preg_replace('/\s/', '', $file), $matches_dvb);
		preg_match_all('/make_channel\({(.*?)}\)/', preg_replace('/\s/', '', $file), $matches_channel);
		$matches["dvb"] = $matches_dvb[0];
		$matches["channel"] = $matches_channel[1];
		//print_r($matches);
		foreach ($matches as $matches_key => $matches_val) {
			foreach ($matches_val as $make_channel) {
				if (preg_match('/id=(\d+)/i', $make_channel, $find_id)) {
					preg_match_all('/([a-zA-Z0-9_]+)=([\+@{}a-zA-Z0-9_.:\"\/\/]+)/', $make_channel, $make_channel_param);
					foreach($make_channel_param[0] as $make_channel_param){
						$make_channel_param_each=explode('=',$make_channel_param);
						//фиксы для dvb_tune
						if ($make_channel_param_each[1] == 'dvb_tune') {
							list($make_channel_param_each[0], $make_channel_param_each[1]) = array($make_channel_param_each[1], $make_channel_param_each[0]);
						}
						//фиксы для make_channel
						if ($make_channel_param_each[0] == 'input') {
							if (preg_match('/dvb:\/\/(.*?)$/i', $make_channel_param_each[1], $input_stream)) {
								$PARAMS[$matches_key][$find_id[1]]["input_stream"]=$input_stream[1];
							}else{
								$PARAMS[$matches_key][$find_id[1]]["input_stream"]='other';
							}
						}
						$PARAMS[$matches_key][$find_id[1]][$make_channel_param_each[0]]=str_replace(array('"','{','}'), "", $make_channel_param_each[1]);
					}	
				}
			}
		}
		return $PARAMS;
	}
	
	public function delete_astra_config($server_conf) {
		if ($this->mysql_res = $this->mysql->query("DELETE From server_conf Where id = '".$server_conf."' ")) {
			return true;
		}else{
			return false;
		}
		return false;
	}
		
		
	public function __destruct() {
		if ($this->mysql_res){
			@$this->mysql_res->close();
		}
		
		if ($this->mysql){
			@$this->mysql->close();
		}
	}
}

?>